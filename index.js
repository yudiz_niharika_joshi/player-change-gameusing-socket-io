const express = require('express')
const app = express()
require('dotenv').config()
const server = require('http').createServer(app)
let users = []

const { Server } = require('socket.io')
const io = new Server(server)
// eslint-disable-next-line no-undef
const PORT = process.env.PORT || 8000
const path = require('path')    
// eslint-disable-next-line no-undef
const publicPath = path.join(__dirname + '/')
// console.log('hi')
console.log(publicPath)
var roomno = Math.floor(1000 + Math.random() * 9000)
console.log(roomno)

io.on('connection', function (socket) {
    console.log('Socket successfully connected with id: ' + socket.id)

    io.on('connection', function (socket) {
        socket.join('room-' + roomno)
        //Send this event to everyone in the room.
        io.sockets
            .in('room-' + roomno)
            .emit('connectToRoom', 'You are in room no. ' + roomno)
    })
    socket.on('join-server', () => {
        const user = {
            id: socket.id,
        }
        users.push(user)
        console.log(users)

        io.emit('new-user', users)
    })
    socket.emit('restInterval',()=>{
        console.log('reset event fired')
    })

    // socket.broadcast.emit(
    //     'change-player',
    //     { session_id: socket.id },
    //     function () {
    //         console.log('emitter')
    //     }
    // )

    socket.on('disconnect', () => {
        users.filter((u) => u.id !== socket.id)
        console.log('user disconnected: ' + socket.id)
    })
})
// eslint-disable-next-line no-undef
app.use(express.static(publicPath))

server.listen(PORT, () => {
    console.log('listening on port: ' + PORT)
})
